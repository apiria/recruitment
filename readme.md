
## Getting Started
You will need understanding of JavaScript, Node.Js, Mongodb and any client side library or framework in order to complete this task.

You have to create a fork of this repository to complete the task. There is no deadline for this task, Once it is done push the changes to the forked repository and share the link with me. The sooner you complete the task, the better as we are screening multiple candidates and if someone responds with a good codebase, we might have to consider him/her first.

We have provided necessary wireframes and Sample data in the repository, Feel free to send me a email if you have any questions and doubt.

### Features

- A User should be able to signup and signin
- Multiple role support for the user(Admin, Manager, User)
- Admin should be able to create user and assign Manager 
- A User should be able to create reimbursement claims 
- Page to show all pending claims
- Manager or Admin can Approve or Reject claims
- User can delete the pending claims
- Use proper validations and error handling 

### Advance Features
- Show all the past claims and option to filter claims based on status and claim type
- User report view which shows the no of claims a user has created, approved claims, Rejected claims etc.